## Status
[![pipeline status](https://gitlab.com/bastillebsd-templates/adguardhome-exporter/badges/main/pipeline.svg)](https://gitlab.com/bastillebsd-templates/adguardhome-exporter/commits/main)

## adguardhome-exporter
Bastille Template for Adguard Home Prometheus Exporter

## Bootstrap
```shell
bastille bootstrap https://gitlab.com/bastillebsd-templates/adguardhome-exporter
```

## Usage
```shell
bastille template TARGET bastillebsd-templates/adguardhome-exporter
```
